
// shared module

export { ConfigurationService,
    APP_CONFIGURATION,
    ApplicationConfiguration,
    ApplicationSettingsConfiguration,
    LocalizationSettingsConfiguration,
    RemoteSettingsConfiguration,
    SettingsConfiguration} from './shared/services/configuration.service';
export { ModalService } from './shared/services/modal.service';
export { DialogComponent, DIALOG_BUTTONS } from './shared/components/modal/dialog.component';
export { MsgboxComponent } from './shared/components/msgbox/msgbox.component';
export { SpinnerComponent } from './shared/components/modal/spinner.component';
export { ToastComponent } from './shared/components/modal/toast.component';
export { ToastService } from './shared/services/toast.service';
export { GradeScale, GradeScaleService, GradePipe, round } from './shared/services/grade-scale.service';
export { LoadingService } from './shared/services/loading.service';
export { LocalizedDatePipe } from './shared/pipes/localized-date.pipe';
export { SemesterPipe } from './shared/pipes/semester.pipe';
export { TemplatePipe } from './shared/pipes/template.pipe';
export { NgVarDirective } from './shared/directives/ngvar.directive';
export { IfLocationDirective } from './shared/directives/if-location.directive';
export { IfServiceDirective } from './shared/directives/if-service.directive';
export { LocalizedAttributesPipe} from './shared/pipes/localized-attributes.pipe';
export { SharedModule } from './shared/shared.module';
export { UserActivityService, UserActivityEntry } from './shared/services/user-activity/user-activity.service';
export { SessionUserActivityService } from './shared/services/session-user-activity/session-user-activity.service';
export {
  PersistentUserActivityService
} from './shared/services/persistent-user-activity/persistent-user-activity.service';
export { DiagnosticsService, ApiServerStatus } from './shared/services/diagnostics.service';
export { AppSidebarService, AppSidebarNavigationItem, SIDEBAR_LOCATIONS } from './shared/services/app-sidebar.service';
export { AppGuestSidebarService, GUEST_SIDEBAR_LOCATIONS } from './shared/services/app-guest-sidebar.service';
export { UserStorageService } from './shared/services/user-storage';
export { RequestTypesService, RequestTypeItem } from './shared/services/request-types/request-types.service';
export { LocalUserStorageService, SessionUserStorageService} from './shared/services/browser-storage.service';
export { FallbackUserStorageService, UserStorageInterface, USER_STORAGE } from './shared/services/fallback-user-storage.service';
export { ExportSpreadsheetService } from './shared/services/export-spreadsheet.service';
export { AppEventService } from './shared/services/app-event.service';
export { CertificateService } from './shared/services/certificate-service/certificate-service.service';
export { SignatureInfoComponent } from './shared/components/signature-info/signature-info.component'
// events module
export { ServerEvent, SERVER_EVENT_SUBSCRIBERS, SERVER_EVENT_CHILD_SUBSCRIBERS, ServerEventService,
  ServerEventSubscriber, ServerEventServiceStatus } from './events/services/server-event.service';
export { ServerEventModule } from './events/server-event.module';
// error module
export { ErrorBaseComponent, HttpErrorComponent } from './error/components/error-base/error-base.component';
export { ApiError, ProfileNotFoundError, RequestNotFoundError, UserProfileNotFoundError } from './error/error.custom';
export { ErrorsHandler } from './error/error.handler';
export { ErrorService } from './error/error.service';
export { ErrorRoutingModule } from './error/error.routing';
export { ErrorModule } from './error/error.module';
export { ReferrerRouteService, ReferrerRouteParams } from './shared/services/referrer-route.service';

// auth module
export { LoginComponent } from './auth/components/login/login.component';
export { LogoutComponent } from './auth/components/logout/logout.component';
export { LocationPermission,
  LocationPermissionAccount, LocationPermissionTarget } from './auth/guards/auth.guard.interfaces';
export * from './auth/guards/auth.guard';
export * from './auth/guards/auth.guard.interfaces';
export { AuthRoutingModule } from './auth/auth.routing';
export { AuthCallbackComponent } from './auth/auth-callback.component';
export { RefreshTokenComponent } from './auth/components/refresh/refresh-token.component';
export * from './auth/auth.module';
export { UserService } from './auth/services/user.service';
export { AuthCallbackResponse, AuthenticationService } from './auth/services/authentication.service';
export { ActivatedUser } from './auth/services/activated-user.service';
export {XmasLoadingService} from './shared/services/xmas-loading.service';
export {XmasSpinnerComponent} from './shared/components/modal/xmas-spinner.component';
// helpers
export { getMemoizeKey, asyncMemoize, memoize, removeMemoizeKey } from './helpers/decorators';
