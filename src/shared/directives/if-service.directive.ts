import { Directive, AfterViewInit, OnDestroy, ViewContainerRef, TemplateRef, Input } from "@angular/core";
import { DiagnosticsService } from "../services/diagnostics.service";
import { Subscription } from "rxjs";

@Directive({
  selector: '[if-service]'
})
export class IfServiceDirective implements AfterViewInit, OnDestroy {
  private $implicit: string;
  subscription: Subscription;

  constructor(
    private view: ViewContainerRef,
    private template: TemplateRef<any>,
    private diagnosticsService: DiagnosticsService
  ) {}

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  @Input('if-service') set assign(value: string) {
    if (value) {
      Object.assign(this, {
        $implicit: value
      });
    }
  }

  public get value() {
    return this.$implicit;
  }

  public set value(value: string) {
    this.$implicit = value;
  }

  ngAfterViewInit(): void {
    this.diagnosticsService.hasService(this.$implicit).then((result) => {
      if (result == true) {
        this.view.createEmbeddedView(this.template);
      } else {
        this.view.clear();
      }
    });
  }

}
