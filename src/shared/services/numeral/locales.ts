export async function registration (_numeral){
  await _numeral.register('locale', 'el', {
    delimiters: {
      thousands: '.',
      decimal: ','
    },
    abbreviations: {
      thousand: 'χιλ',
      million: 'εκ',
      billion: 'δισ',
      trillion: 'τρισ'
    },
    ordinal: function (number) {
      return 'ο';
    },
    currency: {
      symbol: '€'
    }
  });

  await _numeral.register('locale', 'el-gr', {
    delimiters: {
      thousands: '.',
      decimal: ','
    },
    abbreviations: {
      thousand: 'χιλ',
      million: 'εκ',
      billion: 'δισ',
      trillion: 'τρισ'
    },
    ordinal: function (number) {
      return 'ο';
    },
    currency: {
      symbol: '€'
    }
  });

  await _numeral.register('locale', 'cy', {
    delimiters: {
      thousands: '.',
      decimal: ','
    },
    abbreviations: {
      thousand: 'χιλ',
      million: 'εκ',
      billion: 'δισ',
      trillion: 'τρισ'
    },
    ordinal: function (number) {
      return 'ο';
    },
    currency: {
      symbol: '€'
    }
  });

  await _numeral.register('locale', 'cy-cy', {
    delimiters: {
      thousands: '.',
      decimal: ','
    },
    abbreviations: {
      thousand: 'χιλ',
      million: 'εκ',
      billion: 'δισ',
      trillion: 'τρισ'
    },
    ordinal: function (number) {
      return 'ο';
    },
    currency: {
      symbol: '€'
    }
  });
}
