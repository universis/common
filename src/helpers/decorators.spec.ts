import {async, inject, TestBed} from '@angular/core/testing';
import {asyncMemoize, memoize, getMemoizeKey, removeMemoizeKey} from './decorators';
import {CommonModule} from '@angular/common';
import {Injectable} from '@angular/core';
import {TranslateModule, TranslateService} from '@ngx-translate/core';

@Injectable()
class TestService1 {

    constructor(private translateService: TranslateService) {
        //
    }

    private count = 0;

    @asyncMemoize()
    async getItems() {
        return [ 'lemon', 'apple', 'orange' ];
    }

    @memoize()
    getSortedItems() {
        return [ 'lemon', 'apple', 'orange' ].sort( (a, b) => {
              return a > b ? -1 : 1;
        });
    }

    @asyncMemoize()
    async getFunc1(arg1, arg2, arg3) {
        return [ arg1, arg2, arg3 ];
    }

    @asyncMemoize()
    async getFunc2(arg1, arg2, arg3) {
        this.count += 1;
        const currentLang = this.translateService.currentLang;
        return [ arg1, arg2, arg3 ];
    }
}

class TestService2 extends TestService1 {
    @asyncMemoize()
    static async getStaticFunc1() {
        return [ 'lemon', 'apple', 'orange' ];
    }
}

describe('Decorators', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CommonModule,
                TranslateModule.forRoot()
            ],
            declarations: [
            ],
            providers: [
                TestService1
            ]
        }).compileComponents();
    }));

    it('should create instance of TestService1', inject([TestService1], async (testService1: TestService1) => {
        expect(testService1).toBeTruthy();
    }));

    it('should call static async method', inject([TestService1], async (testService1: TestService1) => {
        const items = await TestService2.getStaticFunc1();
        expect(items).toBeTruthy();
        // validate session storage items
        const key = getMemoizeKey(TestService2, 'getStaticFunc1');
        const values = JSON.parse(sessionStorage.getItem(key));
        expect(values).toBeTruthy();
        expect(items[0]).toBe(values[0]);
    }));

    it('should call async method', inject([TestService1], async (testService1: TestService1) => {
        const items = await testService1.getItems();
        expect(items).toBeTruthy();
        // validate session storage items
        const key = getMemoizeKey(testService1, 'getItems');
        const values = JSON.parse(sessionStorage.getItem(key));
        expect(values).toBeTruthy();
        expect(items[0]).toBe(values[0]);
    }));

    it('should call method', inject([TestService1], async (testService1: TestService1) => {
        const items = testService1.getSortedItems();
        expect(items).toBeTruthy();
        // validate session storage items
        const key = getMemoizeKey(testService1, 'getSortedItems');
        const values = JSON.parse(sessionStorage.getItem(key));
        expect(values).toBeTruthy();
        expect(items[0]).toBe(values[0]);
    }));

    it('should call async method with args', inject([TestService1], async (testService1: TestService1) => {
        const items = await testService1.getFunc1('lion', 'tiger', 'monkey');
        expect(items).toBeTruthy();
        // validate session storage items
        const key = getMemoizeKey(testService1, 'getFunc1', 'lion', 'tiger', 'monkey');
        const values = JSON.parse(sessionStorage.getItem(key));
        expect(values).toBeTruthy();
        expect(items[0]).toBe(values[0]);
    }));

    it('should call async method which uses class instance', inject([TestService1], async (testService1: TestService1) => {
        const items = await testService1.getFunc2('lion', 'tiger', 'monkey');
        expect(items).toBeTruthy();
    }));

    it('should call async method with different args', inject([TestService1], async (testService1: TestService1) => {
        const items = await testService1.getFunc1('lion', 'tiger', 'monkey');
        // call again with different args
        await testService1.getFunc1('elephant', 'tiger', 'monkey');
        expect(items).toBeTruthy();
        // validate session storage items
        const key = getMemoizeKey(testService1, 'getFunc1', 'elephant', 'tiger', 'monkey');
        const anotherKey = getMemoizeKey(testService1, 'getFunc1', 'lion', 'tiger', 'monkey');
        expect(key).not.toBe(anotherKey);
        const values = JSON.parse(sessionStorage.getItem(key));
        expect(values).toBeTruthy();
        expect(items[0] === values[0]).toBeFalsy();
    }));

    it('should remove memoized key', inject([TestService1], async (testService1: TestService1) => {
        const items = testService1.getSortedItems();
        expect(items).toBeTruthy();
        // validate session storage items
        const key = getMemoizeKey(testService1, 'getSortedItems');
        // remove item from storage
        removeMemoizeKey(testService1, 'getSortedItems');
        const value = sessionStorage.getItem(key);
        expect(value).toBeFalsy();
    }));

});
