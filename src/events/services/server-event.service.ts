import {Inject, Injectable, InjectionToken, Injector, Type} from '@angular/core';
import {BehaviorSubject, Subject, Subscription} from 'rxjs';
import { EventSourcePolyfill } from 'event-source-polyfill';
import { ConfigurationService } from '../../shared/services/configuration.service';
import { ActivatedUser } from '../../auth/services/activated-user.service';
import { takeUntil } from 'rxjs/operators';
export enum ServerEventServiceStatus {
    Connecting,
    Open,
    Closed
}

declare interface UserSnapshot {
    id?: any;
    name: string;
    token: {
        access_token: string;
        scope: string;
    };
}

export declare interface ServerEvent {
    id?: string;
    additionalType?: string;
    entitySet?: string;
    entityType?: string;
    entityAction?: string;
    target?: any;
    result?: any;
    status?: any;
    error?: any;
    recipient?: string;
    scope?: string;
    dateCreated?: Date;
}

export declare interface ServerEventSubscriber {
    subscribe<T>(event: T): void;
}

declare interface ServerEventServiceConfiguration {
    heartbeatTimeout?: number;
}

export let SERVER_EVENT_SUBSCRIBERS = new InjectionToken<Array<Type<ServerEventSubscriber>>>('server.event.subscribers');

export let SERVER_EVENT_CHILD_SUBSCRIBERS = new InjectionToken<Array<Type<ServerEventSubscriber>>>('server.event.child.subscribers');

@Injectable()
export class ServerEventService {
    public message: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    public error: BehaviorSubject<any> = new BehaviorSubject<any>(null);
    private eventSource: EventSourcePolyfill;
    private _heartbeatTimeout = 120 * 1000;
    private _heartbeatInterval: any;
    private activatedUser: ActivatedUser;
    public subscribers: Map<any, Subscription> = new Map();
    private destroy$: Subject<void> = new Subject<void>();

    constructor(private configuration: ConfigurationService,
        private injector: Injector,
        @Inject(SERVER_EVENT_SUBSCRIBERS) private addSubscribers: Array<Type<ServerEventSubscriber>>) {
            this.configuration.loaded
                .pipe(takeUntil(this.destroy$))
                .subscribe(applicationConfiguration => {
                if (applicationConfiguration != null) {
                    // inject activated user only after config is loaded
                    this.activatedUser = this.injector.get(ActivatedUser);
                    // load
                    this.load();
                    // and destroy subscription
                    this.closeSubscription();
                }
            });
        }

    closeSubscription(): void {
        this.destroy$.next();
        this.destroy$.complete();
    }

    load(): void {
        if (this.configuration.config
            && this.configuration.config.settings
            && this.configuration.config.settings.serverEvent) {
                const serverEvent: ServerEventServiceConfiguration = this.configuration.config.settings.serverEvent;
                if (serverEvent.heartbeatTimeout) {
                    this._heartbeatTimeout = serverEvent.heartbeatTimeout;
                }
        }
        if (Array.isArray(this.addSubscribers)) {
            this.addSubscribers.forEach((addSubscriber) => {
                this.subscribe(addSubscriber);
            });
        }
        this.activatedUser.user.subscribe((user) => {
            if (user == null) {
                if (this.status === ServerEventServiceStatus.Open) {
                    this.close();
                }
            }
            if (user) {
                if (this.status === ServerEventServiceStatus.Open) {
                    this.close();
                }
                this.open();
            }
        });
    }

    /**
     * Adds a server event subscriber
     * @param token
     */
    subscribe(token: Type<ServerEventSubscriber>): void {
        const subscriber: ServerEventSubscriber = this.injector.get<ServerEventSubscriber>(token);
        if (subscriber == null) {
            throw new Error('Server event subscriber cannot be instantiated.');
        }
        if (typeof subscriber.subscribe !== 'function') {
            throw new Error('Expected an instance which implements ServerEventSubscriber.');
        }
        const observer = subscriber.subscribe.bind(subscriber);
        this.subscribers.set(token, this.message.subscribe(observer));
    }

    /**
     * Removes a server event subscriber
     * @param token
     */
    unsubscribe(token: Type<ServerEventSubscriber>): void {
        const subscription: Subscription = this.subscribers.get(token);
        // if subscription is not null
        if (subscription != null) {
            // unsubscribe
            subscription.unsubscribe();
        }
        // remove item
        this.subscribers.delete(token);
    }

    protected get source(): string {
        if (this.configuration.settings &&
            this.configuration.settings.remote &&
            this.configuration.settings.remote.server) {
            return new URL('users/me/events/subscribe', this.configuration.settings.remote.server).toString();
        }
        return '/users/me/events/subscribe';
    }

    protected get heartbeatTimeout(): number {
        return this._heartbeatTimeout;
    }

    get status(): ServerEventServiceStatus {
        if (this.eventSource) {
            return this.eventSource.readyState;
        }
        return ServerEventServiceStatus.Closed;
    }

    async openAsync(): Promise<ServerEventService> {
        return this.open();
    }

    private getUserSync(): UserSnapshot {
        const value = sessionStorage.getItem('currentUser');
        if (value == null) {
            return;
        }
        return JSON.parse(value) as UserSnapshot;
    }

    open(): this {
        if (this.status === ServerEventServiceStatus.Open) {
            return this;
        }
        const user: UserSnapshot = this.getUserSync();
        const headers = {
            'Accept': 'application/json',
            'Authorization': `Bearer ${user.token.access_token}`
        };
        this.eventSource = new EventSourcePolyfill(this.source, {
            heartbeatTimeout: this.heartbeatTimeout,
            headers
        });
        const messageListener = this.onMessage.bind(this);
        const errorListener = this.onError.bind(this);
        this.eventSource.addEventListener('message', messageListener);
        this.eventSource.addEventListener('error', errorListener);
        // set heartbeat interval
        this._heartbeatInterval = setInterval(() => {
            // close
            if (this.eventSource && this.eventSource.readyState !== ServerEventServiceStatus.Closed) {
                this.eventSource.close();
            }
            const user = this.getUserSync();
            if (user == null) {
                return;
            }
            const headers = {
                'Accept': 'application/json',
                'Authorization': `Bearer ${user.token.access_token}`
            };
            // and create new
            this.eventSource = new EventSourcePolyfill(this.source, {
                heartbeatTimeout: this.heartbeatTimeout,
                headers
            });
            // set listeners
            this.eventSource.addEventListener('message', messageListener);
            this.eventSource.addEventListener('error', errorListener);
        }, this.heartbeatTimeout - 5000); // set heartbeat interval
        return this;
    }

    protected onMessage(ev: any): void {
        // emit message
        if (ev.type === 'message') {
            let data: any = null;
            if (typeof ev.data === 'string') {
                data = JSON.parse(ev.data);
            } else if (typeof ev.data === 'object') {
                data = ev.data;
            }
            if (data) {
                this.message.next(data);
            }
        }
    }

    protected onError(ev: any): void {
        // emit error
        if (ev.error) {
            return this.error.next(ev.error);
        }
        return this.error.next(ev);
    }

    close(): boolean {
        if (this.eventSource) {
            this.eventSource.close();
            return true;
        }
        // clear heartbeat interval
        if (this._heartbeatInterval) {
            clearInterval(this._heartbeatInterval);
        }
        return false;
    }

}
